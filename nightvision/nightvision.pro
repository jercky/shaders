<?xml version="1.0"?>
<project version="2">
	<pipeline>
		<pass name="Simple" type="shader" active="true">
			<shader type="vs" path="shaders/SimpleVS.glsl" entry="main" />
			<shader type="ps" path="shaders/SimplePS.glsl" entry="main" />
			<inputlayout>
				<item value="Position" semantic="POSITION" />
				<item value="Normal" semantic="NORMAL" />
				<item value="Texcoord" semantic="TEXCOORD0" />
			</inputlayout>
			<rendertexture />
			<items>
				<item name="Box" type="geometry">
					<type>Cube</type>
					<width>1</width>
					<height>1</height>
					<depth>1</depth>
					<x>0.200545087</x>
					<y>-0.0248206109</y>
					<z>0.0204785205</z>
					<topology>TriangleList</topology>
				</item>
				<item name="bunny" type="model">
					<filepath>models/bunnyLowPoly.obj</filepath>
					<grouponly>false</grouponly>
					<x>0.157855868</x>
					<y>0.796934187</y>
				</item>
				<item name="test" type="model">
					<filepath>models/sword.obj</filepath>
					<grouponly>false</grouponly>
					<scaleX>0.100000001</scaleX>
					<scaleY>0.100000001</scaleY>
					<scaleZ>0.100000001</scaleZ>
					<pitch>1.57079637</pitch>
					<yaw>1.57079637</yaw>
					<x>0.387019396</x>
					<y>0.538387001</y>
					<z>0.432015717</z>
				</item>
			</items>
			<itemvalues>
				<value variable="matVP" for="Box">
					<row>
						<value>5.62292576</value>
						<value>0.0553887933</value>
						<value>0.0470487364</value>
						<value>0.0470393337</value>
					</row>
					<row>
						<value>-8.80982611e-07</value>
						<value>2.16988087</value>
						<value>-0.43845883</value>
						<value>-0.438371181</value>
					</row>
					<row>
						<value>0.294686019</value>
						<value>-1.05687129</value>
						<value>-0.897741735</value>
						<value>-0.897562325</value>
					</row>
					<row>
						<value>0</value>
						<value>0</value>
						<value>4.80097961</value>
						<value>5</value>
					</row>
				</value>
			</itemvalues>
			<variables>
				<variable type="float4x4" name="matVP" system="ViewProjection" />
				<variable type="float4x4" name="matGeo" system="GeometryTransform" />
				<variable type="float" name="elapsedTime" lastframe="true" system="TimeDelta" />
				<variable type="float2" name="mPos" system="MousePosition" />
				<variable type="float2" name="winSize" system="ViewportSize" />
				<variable type="float4" name="modelView" system="CameraPosition" />
			</variables>
			<macros />
		</pass>
	</pipeline>
	<cameras>
		<camera name="test">
			<row>
				<value>0.999390841</value>
				<value>-1.65775404e-07</value>
				<value>-0.0348993316</value>
				<value>-1.49011612e-08</value>
			</row>
			<row>
				<value>-0.0107843298</value>
				<value>0.9510566</value>
				<value>-0.308828771</value>
				<value>-0</value>
			</row>
			<row>
				<value>0.0331912898</value>
				<value>0.309017003</value>
				<value>0.950477242</value>
				<value>-5.00000095</value>
			</row>
			<row>
				<value>0</value>
				<value>0</value>
				<value>0</value>
				<value>1</value>
			</row>
			<row />
		</camera>
	</cameras>
	<settings>
		<entry type="file" name="Simple" shader="ps" />
		<entry type="file" name="Simple" shader="vs" />
		<entry type="camera" fp="false">
			<distance>5</distance>
			<pitch>26</pitch>
			<yaw>177</yaw>
			<roll>360</roll>
		</entry>
		<entry type="clearcolor" r="0" g="0" b="0" a="0" />
		<entry type="usealpha" val="false" />
	</settings>
</project>
