#version 330

const vec3 proportion = vec3(0.30, 0.59, 0.11);

in vec4 color;
out vec4 outColor;

uniform float elapsedTime;
varying vec3 uv;
uniform vec2 mPos;
uniform vec2 winSize;               

float noise (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.1818,78.233)))*
        43758.5453123);
}

float lunettes()
{
	float fac = 0.15;
	vec2 c = gl_FragCoord.xy;
	c.x /= winSize.x;
	c.y /= winSize.y;
	
	vec2 ratio;
	ratio.x = min(1,winSize.y/winSize.x);
	ratio.y = min(1,winSize.x/winSize.y);
	
	float lun_tai = 0.12;
	vec2 lun_dis;
	lun_dis.x = 0.10*ratio.x;
	float d1 = distance(c/ratio,(mPos.xy-lun_dis)/ratio);
	float d2 = distance(c/ratio,(mPos.xy+lun_dis)/ratio);
	if (d1 < lun_tai || d2 < lun_tai)
		//if (all(lessThan(c,  mPos.xy+0.1*ratio)) && all(greaterThan(c,  mPos.xy-0.1*ratio)) )
		fac = 1.0;
	return fac;
}

float cellShading(float v)
{
	float r = 0.75;
	//if (v < .9) r = .8;
	//if (v < .8) r = .7;
	if (v < .75) r = .5;
	//if (v < .6) r = .5;
	if (v < .5) r = .25;
	//if (v < .4) r = .3;
	//if (v < .3) r = .2;
	if (v < .25) r = .05;
	//if (v < .1) r = .0;
	
	return r;
		
}

vec3 nightVisionColor(float fac)
{
	vec2 r = uv.xy;
	r.x *= sin(elapsedTime);
	r.y *= cos(elapsedTime);   
	float vision = color.r * proportion.x + 
				   color.g * proportion.y + 
				   color.b * proportion.z 
				   + noise(r) * 0.17;
	vision = cellShading(vision);
	vision *= step(0.2,vision) *0.9 * fac;				   	   	   
	vec3 nightVision = vec3 (0., vision, 0.);	
	return nightVision;
}

void main() {
	float fac = lunettes();
	vec3 nightVision = nightVisionColor(fac);
   outColor = vec4(nightVision,1.0);
}