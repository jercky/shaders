#version 330

uniform mat4 matVP;
uniform mat4 matGeo;

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;

uniform float elapsedTime;
varying vec3 uv;

out vec4 color;

uniform vec2 mPos;
uniform vec2 winSize;

uniform vec4 modelView;

void main() {
	uv = pos;
   color = vec4(abs(normal), 1.0);
   gl_Position = matVP * matGeo * vec4(pos, 1);
}
