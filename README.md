# Shaders

Écrits et testés avec [SHADERed](https://github.com/dfranx/SHADERed).

## Pseudo-Night Vision
- Passage en nuances de verts avec la chrominance ;
- Pseudo-cellshading ;
- Bruit dynamique ;
- Effet de lunettes suivants la souris.

![](nightvision/nightvision.gif)
